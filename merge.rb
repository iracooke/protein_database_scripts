#!/usr/bin/env ruby

# Merges fasta files checking for duplicates
# First entry found is the entry that is used
#

require 'bio'
require 'set'

entry_ids=Set.new

ARGV.each do |fasta_file|  

	file = Bio::FastaFormat.open(fasta_file.chomp)
	file.each do |entry|
		unless entry_ids.include? entry.entry_id
			$stdout.write entry
			entry_ids << entry.entry_id
		end
	end
end