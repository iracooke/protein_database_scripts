#!/usr/bin/env ruby

# Filters a fasta file so only entries matching a condition are emitted
#

require 'bio'
require 'optparse'

# User friendly command-line options
#
options = {}
optparser=OptionParser.new do |opts|
	opts.banner = "Usage: filter_fasta.rb [options] file.fasta file2.fasta"

	opts.on("-d filter", "--definition filter ", "Filter on entry definition") do |f|
		options[:definition_filter] = f
	end

	options[:invert] = false
	opts.on("-i", "--invert", "Invert Filter") do
		options[:invert] = true
	end

	opts.on("-f filename", "--idfile filename ", "Filter on identifiers in a file") do |f|
		options[:refseqid_filter] = f
	end

	options[:filter_regex] = "ref\|(.*?)\|"
	opts.on("--filter-regex re", "Filter regex ") do |re|
		options[:filter_regex] = re
	end

	opts.on_tail("-h", "--help", "Show this message") do
		puts opts
		exit
	end

end

optparser.parse!

# Checking that the user at least supplied the name of one fasta file
#
if ARGV.length==0
	puts optparser 
	exit
end

$refseq_filter_ids = ""
if options[:refseqid_filter] && File.exists?(options[:refseqid_filter])
	$refseq_filter_ids = File.read(options[:refseqid_filter])
end

def passes_filters(entry,options)

	if options[:definition_filter]


		if entry.definition =~ /#{options[:definition_filter]}/
			return true
		else
			return false
		end
	end

	# require 'debugger';debugger

	if $refseq_filter_ids.length > 0
		# require 'debugger';debugger


		if entry.definition =~ /#{options[:filter_regex]}/
			refseq_id = entry.definition.match(/#{options[:filter_regex]}/)[1]			

			return false if refseq_id==""

			if $refseq_filter_ids =~ /#{refseq_id}/
				return true
			else
				return false
			end
		else
			return false
		end
	end

	return true

end


ARGV.each do |fasta_file|  

	file = Bio::FastaFormat.open(fasta_file.chomp)
	file.each do |entry|


		pass = passes_filters(entry,options)
		pass = !pass if options[:invert]
		if pass
			$stdout.write entry
		end
	end
end