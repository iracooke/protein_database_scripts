# Update scripts for LIMS Proteomics databases

## DBs Managed Exclusively by Mascot

`SwissProt` `NCBInr`


## Uniprot Taxon specific

All include cRAP DB as well as taxonomy specific Uniprot download

`DogCowHuman` http://www.uniprot.org/uniprot/?query=taxonomy%3a9913+OR+taxonomy%3a9606+OR+taxonomy%3a9615&force=yes&format=fasta

`DogMouse` http://www.uniprot.org/uniprot/?query=taxonomy%3a9615+OR+taxonomy%3a10090&force=yes&format=fasta

`UniprotDog` http://www.uniprot.org/uniprot/?query=taxonomy%3a9615&force=yes&format=fasta

`UniprotHuman` http://www.uniprot.org/uniprot/?query=taxonomy%3a9606&force=yes&format=fasta

`UniprotHumanCow` http://www.uniprot.org/uniprot/?query=taxonomy%3a9606+OR+taxonomy%3a9613&force=yes&format=fasta

`UniprotChicken` http://www.uniprot.org/uniprot/?query=taxonomy%3a9031&force=yes&format=fasta

`UniprotMouse` http://www.uniprot.org/uniprot/?query=taxonomy%3a10090&force=yes&format=fasta

`UniprotWheat` http://www.uniprot.org/uniprot/?query=taxonomy%3a4565&force=yes&format=fasta

To create latest versions of these run commands like this (eg for Human)

The first step is to create a swissprotified version of the crap db

```bash
	curl ftp://ftp.thegpm.org/fasta/cRAP/crap.fasta > crap.fasta
	cat crap.fasta | grep '>' | awk -F '\|' '{print $2}' > crap_spids.txt
```

Then just retrieve those manually from uniprot.org and save them under `crap_swissprot.fasta`

```bash
	curl 'http://www.uniprot.org/uniprot/?query=taxonomy%3a9606&force=yes&format=fasta' > human.fasta

	./merge.rb human.fasta crap_swissprot.fasta > UniprotHuman_20140716.fasta
```

## Uniref Clusters

**Uniref90InfluenzaA**

This retrieves all entries for all clusters mapped to Uniprot identifiers

```bash
	curl 'http://www.uniprot.org/uniprot/?query=cluster%3a(uniprot%3a(organism%3a%22Influenza+A+virus+%5b11320%5d%22)+AND+identity%3a0.9)&format=fasta' > influenzaAref90.fasta
```

This retrieves cluster representatives as UniRef90 identifiers

```bash
	curl 'http://www.uniprot.org/uniref/?query=uniprot:(organism%3A%22Influenza+A+virus+%5B11320%5D%22)+identity:0.9&format=fasta' > fluA90.fasta
```

## FhD with Sheep and Rat

First grab sheep and rat from Uniprot

```bash
	curl 'http://www.uniprot.org/uniprot/?query=taxonomy%3a10116+OR+taxonomy%3a9940&force=yes&format=fasta' > ratsheep.fasta
```

Merge with crap and FhD

```bash
	./merge.rb ratsheep.fasta crap_swissprot.fasta ../fhepatica/FHD.fasta > FHD_20150825.fasta
```

## RefSeq

The first step with both these refseq databases is to download the full set of vertebrate protein entries.  Both `refseq_human` and `refseq_mammal` are compiled as subsets of these.

```bash
	wget ftp://ftp.ncbi.nlm.nih.gov/refseq/release/vertebrate_mammalian/vertebrate*.protein.faa.gz
	gunzip *.faa.gz
```

Now make a contaminants file using the same style of identifiers.  To do this lookup RefSeq identifiers from uniprot.org for all the identifiers in crap_spids.txt.  These are in `refseq_crap.txt`.  Then create a fasta file with these

```bash
	./filter_fasta.rb -f refseq_crap.txt vertebrate_mammalian.*.protein.faa > refseq_crap.fasta
```

Since some contaminants are non-mammal I looked these up manually and put them in a special file `nonmammal_crap_refseq.fasta`

`refseq_human`

(Only entries matching `Homo sapiens`)

```bash
	./filter_fasta.rb -d '[Hh]omo [Ss]apiens' vertebrate_mammalian.*.protein.faa > refseq_human.fasta
	./merge.rb refseq_human.fasta refseq_crap.fasta nonmammal_crap_refseq.fasta > refseq_human_20140716.fasta
```


`refseq_cow`

(Only entries matching `Bos taurus`)

```bash
	./filter_fasta.rb -d '[Bb]os [Tt]aurus' vertebrate_mammalian.*.protein.faa > refseq_cow.fasta
	./merge.rb refseq_cow.fasta refseq_crap.fasta nonmammal_crap_refseq.fasta > refseq_cow_20150318.fasta
```

`refseq_mouse`

(Only entries matching `Mus musculus`)

```bash
	./filter_fasta.rb -d '[Mm]us [Mm]usculus' vertebrate_mammalian.*.protein.faa > refseq_mouse.fasta
	./merge.rb refseq_mouse.fasta refseq_crap.fasta nonmammal_crap_refseq.fasta > refseq_mouse_20150318.fasta
```


`refseq_mammal`

(All entries)

```bash
	./filter_fasta.rb vertebrate_mammalian.*.protein.faa > refseq_mammal.fasta
	./merge.rb refseq_mammal.fasta nonmammal_crap_refseq.fasta > refseq_mammal_20140716.fasta
```

`refseq_yeast`

(All entries)

```bash
	wget ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/fungi/Saccharomyces_cerevisiae/latest_assembly_versions/GCF_000146045.2_R64-1-1/GCF_000146045.2_R64-1-1_protein.faa.gz
	gunzip GCF_000146045.2_R64-1-1_protein.faa.gz
	./filter_fasta.rb GCF_000146045.2_R64-1-1_protein.faa > refseq_yeast.fasta
	./merge.rb refseq_crap.fasta refseq_yeast.fasta > refseq_yeast_20150601.fasta
```

## Protk

sphuman spmouse
